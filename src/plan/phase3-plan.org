#+TITLE: Phase 3 - Development and Delivery Plam
#+AUTHOR: VLEAD
#+DATE: [2019-08-07 Wed]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document looks at the technical realization of the
deliverables for the phase 3 and beyond.

* Deliverables
** Development of Experiments
*** Authoring Platform
*** Development of Artifacts
*** Collection of Telemetry

** Hosting of Labs
*** Creation of CI/CD platform
*** Replication of CI/CD platfrom
    1. On Testing and Staging Environments
    2. On Production (AWS)
    3. On Baadal

** Portals
*** Outreach Portal
*** VLEAD Dev Portal

** Evolution of Virtual Labs
*** Componetization
*** Support for Authoring in multiple markup languages

* Team
  1. Sadhana
  2. Niranjan
  3. Pawan
  4. Sravanthi
  5. Baalamma
  6. Sidharth (RA)
  7. Ojas Mohril
  8. Thirumal Ravula
  9. Mrityunjay
  10. Priya
  11. Ravi Shankar
  12. Venkatesh Choppella
