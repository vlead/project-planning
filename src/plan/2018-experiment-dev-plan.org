#+title:  VLEAD Experiment Development 2018 
#+author: VLEAD 
#+date: [2018-07-23 Mon]
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t
#+OPTIONS: arch:headline author:t c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil prop:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t title:t toc:nil
#+OPTIONS: todo:t |:t
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.3.1 (Org mode 8.3.4)


* Revision History 
  [2018-07-23 Mon] - First Draft - Priya Raman

* Introduction
  Virtual Labs Phase III DPR states that by the end Mar 2020
  23 experiments are developed and hosted by IIITH for use
  by students across the world.  To meet this objective,
  VLEAD decided to run an internship program in the summer
  of 2018 for the students of IIIT-Hyd.  At the end of this
  internship approx 70 % of 8 experiments in Data Structures
  Lab were developed.  This document aims to detail the plan
  for the coming months ( Aug 2018 - Feb 2020 ) towards
  realizing the deliverable of 23 experiments.

* Deliverable as per DPR 
  
  |---------------+--------------+--------------+--------------+-------+---------------|
  | Item          | Year 1       | Year 2       | Year 3       | Total | Ref           |
  |               | (Dec, 2017   | (Apr, 2018   | (Apr, 2019   |       |               |
  |               | - Mar, 2018) | - Mar, 2019) | - Mar, 2020) |       |               |
  |---------------+--------------+--------------+--------------+-------+---------------|
  | Number of new | 4            | 7            | 12           |    23 | Pg 14 of      |
  | experiments   |              |              |              |       | [[https://drive.google.com/open?id=0B1SJVUBLNVcZUS12ZzRGOVpTNmcwUHNlSm5WMGliRDkya0ZB][Phase III DPR]] |
  |---------------+--------------+--------------+--------------+-------+---------------|

* Allocated Budget as per DPR

  |----------------+--------------+--------------+--------------+----------+---------------|
  | Item           | Year 1       | Year 2       | Year 3       | Total    | Remarks &     |
  |                | (Dec, 2017   | (Apr, 2018   | (Apr, 2019   |          | Ref           |
  |                | - Mar, 2018) | - Mar, 2019) | - Mar, 2020) |          |               |
  |                | in Lakhs     | in Lakhs     | in Lakhs     | in Lakhs |               |
  |----------------+--------------+--------------+--------------+----------+---------------|
  | New Experiment | 23           | 36           | 54.25        | 113.25   | Development + |
  | Development    | (8 + 15)     | (14 + 22)    | (24+30.25)   |          | Manpower      |
  |                |              |              |              |          | Pg 21 of      |
  |                |              |              |              |          | [[https://drive.google.com/open?id=0B1SJVUBLNVcZUS12ZzRGOVpTNmcwUHNlSm5WMGliRDkya0ZB][Phase III DPR]] |
  |----------------+--------------+--------------+--------------+----------+---------------|
  
* Overall Plan

  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | Item                            | Start Date       | End Date         | # of completed | Repository     | Remarks                  |
  |                                 |                  |                  |    experiments | Link and Plan/ |                          |
  |                                 |                  |                  |                | Status Link    |                          |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | One experiment from the         | [2018-07-20 Fri] | [2018-08-20 Mon] |                | infra          | Thirumal/Sadhana         |
  | 8 exp developed during          |                  |                  |              1 | [[https://gitlab.com/groups/vlead-projects/experiments/infra/-/milestones/2][milestone]]      | to add the               |
  | internship to be taken          |                  |                  |                | [[https://gitlab.com/vlead-projects/experiments/infra/styles/blob/master/src/landing-page/index.org][requirement]]    | link to milestones       |
  | to application stage            |                  |                  |                |                | & Realization Plan       |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | Remaining 7  experiments        | [2018-08-21 Tue] | [2018-11-30 Fri] |              7 |                | Thirumal/Sadhana         |
  | developed during                |                  |                  |                |                | to add the               |
  | 2018 - summer internship        |                  |                  |                |                | link to milestones       |
  | to be taken to application      |                  |                  |                |                | & Realization Plan       |
  | stage                           |                  |                  |                |                |                          |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | SSAD - 2018 Monsoon Term        | [2018-08-01 Wed] | [2018-11-30 Fri] |                |                | 4 old experiments        |
  |                                 |                  |                  |                |                | *  Heap sort/ Quick sort |
  |                                 |                  |                  |                |                | * Linked Lists           |
  |                                 |                  |                  |                |                | * Infix to Postfix       |
  |                                 |                  |                  |                |                | * Stacks and Queues      |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | POPL Course - 2018              |                  |                  |                |                | 10 new experiments       |
  |                                 |                  |                  |                |                | to be built by students  |
  |                                 |                  |                  |                |                | as part of their course  |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | Winter Internship               | [2018-12-03 Mon] | [2018-12-31 Mon] |                |                | 5 new experiments        |
  | 2018 - IIITH                    |                  |                  |                |                |                          |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | 10 experiments of 20  developed | [2019-01-01 Tue] | [2019-04-30 Tue] |             10 |                |                          |
  | during 2018 Winter Internship   |                  |                  |                |                |                          |
  | POPL & SSAD to be taken to      |                  |                  |                |                |                          |
  | application stage               |                  |                  |                |                |                          |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | Summer Internship               | [2018-05-20 Mon] | [2018-07-19 Fri] |                |                | 10 new experiments       |
  | 2019 - IIITH                    |                  |                  |                |                |                          |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | 5 experiments of 10  developed  | [2019-08-01 Thu] | [2019-11-28 Thu] |              5 |                |                          |
  | during 2019 Summer Internship   |                  |                  |                |                |                          |
  | to be taken to                  |                  |                  |                |                |                          |
  | application stage               |                  |                  |                |                |                          |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|
  | Winter Internship               | [2019-12-02 Mon] | [2019-12-31 Tue] |                |                | Buffer / Clean up        |
  | 2019 - IIITH                    |                  |                  |                |                | Services                 |
  |---------------------------------+------------------+------------------+----------------+----------------+--------------------------|

* Evolution 
** MoM on [2018-07-26 Thu]
   Prof. Venkatesh, Thirumal & Priya
   Following decisions were made : 
   
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|
   | S.No | Resource      | FT/PT      | Task                       | Remarks     | End Date | Action Item           |
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|
   |   1. | VLEAD Staff   | Full Time  | 1 exp to application stage | Sadhana,    |          | Come up with the      |
   |      |               |            |                            | Anirudh &   |          | release / final       |
   |      |               |            |                            | Thirumal    |          | demo date             |
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|
   |   2. | SSAD Students | Part Time  | 5 exp to application stage | SSAD Team   |          | Awaiting allotment    |
   |      |               | 5 hrs/week | (Data structures lab)      | ( 5 teams - |          |                       |
   |      |               |            |                            | 3/team )    |          |                       |
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|
   |   3. | POPL TAs      | Part Time  | 10 POPL experiments        | 2 TAs       |          | Thirumal & Venkatesh  |
   |      |               | 5hrs/week  |                            | Sidharth &  |          | to meet the students  |
   |      |               |            |                            | Nitin       |          | on [2018-07-30 Mon]   |
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|
   |   4. | Btech/ Mtech  | Full Time  | Services + 2 Experiments   | 4-5 interns |          | Draft mail from Lalit |
   |      | interns       |            | to application stage       | from BITS/  |          | Contacts from Lalit   |
   |      |               |            |                            | JNTU/OU     |          |                       |
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|
   |   5. | Winterns      | Full Time  | 18 experiments to          | IIIT-H      |          | Task 1 to 3 should    |
   |      |               |            | application stage          | interns     |          | be completed before   |
   |      |               |            |                            |             |          | [2018-12-01]          |
   |------+---------------+------------+----------------------------+-------------+----------+-----------------------|

   The overall idea is to get 18 experiments to application
   stage before the end of 2018. 

** MoM on [2018-08-09 Thu]
   10:00 AM
   Attendees : Priya, MK

   |------+---------------+------------+----------------------------+-------------+----------+----------------------|
   | S.No | Resource      | FT/PT      | Task                       | Remarks     | End Date | Action Item          |
   |------+---------------+------------+----------------------------+-------------+----------+----------------------|
   |   1. | VLEAD Staff   | Full Time  | Shell as per the           | Sadhana,    |          | [[https://gitlab.com/groups/vlead-projects/experiments/infra/-/milestones/2][First milestone]]      |
   |      |               |            | [[https://gitlab.com/vlead-projects/experiments/extopia/requirements/blob/master/src/index.org][requirements]]               | Anirudh &   |          | Second Milestone     |
   |      |               |            |                            | Thirumal    |          | demo date            |
   |------+---------------+------------+----------------------------+-------------+----------+----------------------|
   |   2. | SSAD Students | Part Time  | 4 exp to application stage | SSAD Team   |          | * Kick Off meet      |
   |      |               | 5 hrs/week | * Heap sort/ Quick sort    | ( 4 teams - |          | on Aug 10th 2018     |
   |      |               |            | * Linked List              | 4/team )    |          |                      |
   |      |               |            | * Infix to Postfix         |             |          | * MK to come up      |
   |      |               |            | * Stacks & Queues          |             |          | with a req doc for   |
   |      |               |            |                            |             |          | all 4 exp            |
   |------+---------------+------------+----------------------------+-------------+----------+----------------------|
   |   3. | POPL TAs      | Part Time  | 10 POPL experiments        | 2 TAs       |          | Thirumal & Venkatesh |
   |      |               | 5hrs/week  |                            | Sidharth &  |          | to meet the students |
   |      |               |            |                            | Nitin       |          | on [2018-07-30 Mon]  |
   |------+---------------+------------+----------------------------+-------------+----------+----------------------|
   |   4. | Btech/ Mtech  | Full Time  | Services + 2 Experiments   | 4-5 interns |          | Meeting on with BITS |
   |      | interns       |            | to application stage       | from BITS/  |          | on  [2018-08-09 Thu] |
   |      |               |            |                            | JNTU/OU/HCU |          |                      |
   |------+---------------+------------+----------------------------+-------------+----------+----------------------|
   |   5. | Winterns      | Full Time  | 18 experiments to          | IIIT-H      |          | Task 1 to 3 should   |
   |      |               |            | application stage          | interns     |          | be completed before  |
   |      |               |            |                            |             |          | [2018-12-01]         |
   |------+---------------+------------+----------------------------+-------------+----------+----------------------|

   - Is item 3 still valid ? 
   - Is 18 still valid ?
   - 1 + part of 2 ( Linked List or Heap Sort ) = Project OneX
   - Task 1 ( Shell ) - End of Aug 
   - Date for Project OneX - Oct 1st 2018 

** MoM on [2018-08-30 Thu]
   Attendees : Thirumal, MK, Prof Venkatesh, Priya
   
   - Release Plan for infra ( immediate ) 
   - Set of features per release   ( immediate )
   - Make a release and understand where we are wrt to the
     OneX project. ( immediate )
   - Bottle necks - Productive developers ? and what is the
     road map 
   - Plan - 
     - Could we offer this as a course for IIITH students ?
     - Could we team up with Talent Sprint to offer a paid
       internship ?
   - First 3 points to be addressed asap. 
   - It was decided that Nodal centres at this point cannot
     be involved in experiment development since we do not
     have a version that can act as "gold standard"
** MoM on [2018-10-09 Tue]
   Based on [[https://gitlab.com/vlead/project-planning/issues/37][several]] meetings between Sept 3rd week and Oct
   first week of 2018 it was [[https://gitlab.com/vlead/project-planning/issues/38][decided]] on [2018-09-27 Thu]
   that a simple model of infra will be built from the
   scratch which will accept an experiment in a std html
   format. This will be rendered by a 'ToC renderer'making
   use of the APIs exposed by Exp DOM.  Based on that the
   following tasks were identified

   - Definition of the exp spec
   - Implementation of APIs in Exp DOM 
   - ToC Renderer
   - Conversion of Merge sort and Heap sort exp to std HTML
     format
   - Build process to support the above
   - Make a release of the old version of infra with changes
     to ToC renderer ( responsive + scroll bar + One X
     requirement)

  It was estimated that this effort will take no more than a
  couple of days with the following proposed Timeline 
  and teams -  
  - Current infra Milestone release - TOC renderer + Mergesort to baseline - 27th Sept 2018
  - Internal release ( Renderer to use the newly defined AST )  - Release on Monday Oct 1st 2018
  - HTML spec to be ready by next Thursday 4th of Oct 2018 ( next meeting) - Infra Team
  - Infra Team - Prof Venaktesh & Thirumal 
  - Exp Dev Team -  Sadhana, Niranjan & Pavan
  
  Actual Timeline - 

   

  - [2018-09-28 Fri] - Start of the effort 
  - [2018-09-30 Sun] - Exp spec in std html format published
                       by Prof Venkatesh
  - [2018-10-01 Mon] - Design discussions with team 
  - [2018-10-03 Wed] - Work started on Exp DOM design -
                       Sadhana
  - [2018-10-04 Thu] - Discussion with MK and stub
                       implementaion of ToC renderer 
                       and  Exp DOM
  - [2018-10-05 Fri] - Review with MK on stubs
                       implementation.
                       Design discussion with Prof Venkatesh
  - [2018-10-08 Mon] - Allocation of the above tasks during
                       the morning meeting as captured in [[https://gitlab.com/vlead-projects/experiments/sys-exp/meetings/blob/master/mom_10-9-2018.org][MoM]]
  - [2018-10-09 Tue] - Meeting with Prof, MK and Priya on 
                       immediate plan 
                        - Definition of the exp spec - *DONE*
                        - Implementation of APIs in Exp DOM
                          *To be done before  [2018-10-10 Wed]*
                        - ToC Renderer *To be done before  [2018-10-10 Wed]*
                        - Conversion of Merge sort and Heap sort exp to std HTML
                          format *To be done before  [2018-10-10 Wed]*
                        - Build process to support the above *To be done before  [2018-10-10 Wed]*
                        - Make a release of the old version of infra with changes
                          to ToC renderer ( responsive + scroll bar + One X
                          requirement)
                          *Made a release on [2018-10-11 Thu]*

                                  

