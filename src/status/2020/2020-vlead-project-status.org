#+title:  VLEAD Project Status 2020
#+author: VLEAD 
#+date: [2020-01-01 Wed]
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t
#+OPTIONS: arch:headline author:t c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil prop:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t title:t toc:nil
#+OPTIONS: todo:t |:t
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.3.1 (Org mode 8.3.4)


* Revision History 
  [2020-01-01 Wed] - First Draft - Priya Raman


* VLEAD Status as on Feb 5th 2020

** Extended Phase II
*** Outreach
**** Completed 
     1. Usage till date 1,71,607 of 73 workshops against a
        target of 91,800 of 25 workshops for the duration
        Jan 2018 - Mar,2020.
     2. 22 Nodal Centers against a target of 25 NC. 3 in
        pipeline.

**** In Progress 
     1. Routine outreach activity - Contacting NC, Visiting
        colleges, Conducting Workshops ( Puttaparthi, Bharat
        Engineering Ibrahim Patnam)
     Status as on 5th Jan 2020 - Slide 3 of [[https://docs.google.com/presentation/d/16m3tvofNV29UtbpFaNv1veRyWuBj8BS8S1bnGHDPo9k/edit#slide=id.g6e986f8711_0_0][link]]
     - Plan for Feb :: 
       1.  Slide 4 of [[https://docs.google.com/presentation/d/16m3tvofNV29UtbpFaNv1veRyWuBj8BS8S1bnGHDPo9k/edit#slide=id.g6e986f8711_0_0][link]]
**** Deadline - March 2020
**** Assignee - Ravi & Mrudvika

*** Integration and Cloud activities - hosting related, maintenance, systems support
**** Completed 
    Apart from continuous monitoring for availability,
    following are the tasks accomplished by Systems team
    1. Reduced Ebs size for EC2 instances on AWS by 700 GB
    2. Hosted Dev pages and experiments on AWS
    3. Deployed Outreach portal and created a test machine
       for the same on AWS
    4. Added couple of scripts(logwatch and mysql
       start/stop) and Monitored outreach portal
       availability
    5. Added logwatch in all the VMs of AWS
    6. Renewed SSL certificates 
    7. Hosted and Modified the UI of Baadal
    8. Resolved base machine server issues (added hard disks
       and restart all the containers) regularly.
    9. Merged and deployed 5 labs on AWS with the made
       changes (issues fixed)
   10. Mounted S3 bucket with outreach EC2 instance.
   11. 3 Servers installation -
**** In Progress 
   1. Post installation - Virtualize with KVM and host all 
      Virtual Labs instances in Ubuntu 18.04 - Stalled
   2. AWS fixes 
   3. Cluster Evolution
   - Plan for Feb :: 
    None 
***** Assignee - Pavan
 
*** Hosting of new experiments
**** Reference - Point  6 [[https://drive.google.com/file/d/1_-soK9nSYnrOBMoY2_Pm0iZYGoPT5rZp/view][mom]] of  16th Sept 2019 
               - It was decided that the steps involved in
                 the hosting of a new lab onto the common
                 server will be shared by IIIT Hyderabad
                 team and the difficulties in carrying out
                 the migration will be shared with IIT
                 Bombay team, if any. Initially, any 5-new
                 labs will be hosted as a pilot before the
                 next PICs meeting (tentatively 7th Oct
                 2019). [Actionable: IIITH]
**** Status External 

      |------+------------------+--------------------------------+------------+------------------------+---------|
      | S.No | Date             | Purpose                        | Sent       | Links                  | Remarks |
      |      |                  |                                | By         |                        |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|
      |   1. | [2019-10-04 Fri] | Kick off meeting with IITB     | Thirumal   | [[https://gitlab.com/vlead-systems/host-experiments/blob/develop/src/realization-plan/mom/2019-10-04.org][mom link]]               |         |
      |      |                  |                                | IITH Hyd   |                        |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|
      |   2. | [2019-10-09 Wed] | A sample lab hosted and        | Thirumal   | [[https://drive.google.com/open?id=1amfFGxy9s2I1phj-v2LBd4umuafIRSml][email link]]             |         |
      |      |                  | communicated with IITB and     | IIITH Hyd  |                        |         |
      |      |                  | requested urls to the sources  |            |                        |         |
      |      |                  | of all the experiments that    |            |                        |         |
      |      |                  | need hosting.                  |            |                        |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|
      |   3. | [2019-10-15 Tue] | Email sent to IITB along       | Thirumal   | [[https://drive.google.com/open?id=1iAISXUvZpDq0v-AFGQXYY2oAu3GBuEO7][email link]]             |         |
      |      |                  | with the work flow for hosting | IIITH Hyd  | [[https://gitlab.com/vlead-systems/host-experiments/blob/develop/src/runtime/wf.org][doc link]]               |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|
      |   4. | [2019-10-25 Fri] | Shared minutes and hosting     | Thirumal   | [[https://drive.google.com/open?id=1K8_nxDwamDx-TEUg-syy_rFuZIoo9txo][email link]]             |         |
      |      |                  | implementation link            | IIITH Hyd  | [[https://gitlab.com/vlead-systems/host-experiments/blob/develop/src/runtime/host-experiments.org][hosting implementation]] |         |
      |      |                  | and readiness of IITB for      |            | [[https://gitlab.com/vlead-systems/host-experiments/blob/develop/src/realization-plan/mom/2019-10-18.org][18th Oct 2019 mom]]      |         |
      |      |                  | final hosting                  |            |                        |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|
      |   5. | [2019-12-22 Sun] | Experiment hosting requests    | Thirumal   | [[https://drive.google.com/open?id=1lCRPi-iap_yejtitGZVzKZcNCqesSPwa][email link]]             |         |
      |      |                  | made by IITB for the period    | IIITH Hyd  |                        |         |
      |      |                  | Oct-Dec 2019                   | Jai Mathur |                        |         |
      |      |                  |                                | IITB       |                        |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|
      |   6. | [2020-01-29 Wed] | Sent the first version of the  | Priya      | [[https://drive.google.com/open?id=1UlKaH7fnsJOlX9A1mIyvcrZZRzOysFrn][email link]]             |         |
      |      |                  | hosting process to IITB for    | IIITH, Hyd |                        |         |
      |      |                  | review                         |            |                        |         |
      |------+------------------+--------------------------------+------------+------------------------+---------|

**** Status Internal
      
      1. 97 of 99 labs hosted on AWS tagged and deployed - Done 
      2. Consolidated lab status sheet consisting of deployment
      status, tag, date etc prepared - In progress
      3. Hosting process - Vanilla version finalized with
         IITB - In Progress  
      - Issues :: 
           1. Lab repo clubbing related experiments is
              needed to capture lab level analytics.
           2. A well defined hosting process is also needed
              to ensure ownership/collection of
              stats/tagging of hosted code etc.
           3. We do not have time to explore CI/CD at this
              point of time and since Phase 3 end date is
              drawing close it will be more productive for
              us to ponder on the proposal made below to
              IITB as a short term hosting solution.

      - Proposal to be made to IITB on 7th Feb 2020 :: 
           1. Lab level UI to be the same across all Phase 3
              experiments and it will be UI 3.0. Link to
              source code ( @Pushpdeep Mishra , you should
              be able to access this repository)
           
           2. Until Phase 3 is done ( 31st March 2020)
              hosting process will be manual and as detailed
              in the shared link .

           3. VLEAD proposes the following process for the
              hosting of labs which have been developed with
              each experiment having its own
              repository. This process on consensus from
              IITB will get updated in the hosting document.
     
               - Step 1 :: IIT B will need to raise a
                           on-boarding request for each lab
                           to be hosted. This onboarding
                           request of type Phase III
                           On-Boarding Request will contain
                           placeholders for the lab
                           repository Name and experiments
                           names and corresponding
                           repositories.  This will need to
                           be filled by IITB.
      
               - Step 2 :: As a response to this request,
                           VLEAD will share the create a lab
                           repository, host all the
                           experiments and populate the lab
                           repo with the list of experiments
                           and their hosted urls. Thi slab
                           repository will be shared with
                           IITB as a response to the
                           on-boarding request.

               - Step 3 :: IIT B will need to populate the
                           lab repository with the common
                           lab information - Introduction,
                           Objective, Target Audience and
                           Course Alignment. These files
                           will be present in the labb
                           repository and IITB will only
                           need to add the content to the
                           respective files. Once this is
                           done IITB will need to raise a
                           Hosting Request.
 
               - Step 4 :: VLEAD will host the lab and the
                           experiments and share the hosted
                           lab link.
       
              All other steps will remain as they are
              detailed in the document.

              4. 50 experiments ( 5 labs ) that have been
                 developed in Phase 2 format ( each lab
                 repository containing its experiments) will
                 be hosted as per the general process
                 defined in the document.

              5. IITB and VLEAD to talk again to close the
                 above proposed model.  Pushpdeep to share a
                 convenient time for 31st Jan 2020 ( AM)

     - Plan for Feb :: 

       1. Get concensus from IIT B on proposed hosting
          process
       2. Follow the process and create a lab repository for
          every on-boarding request received from IITB.
       3. Start hosting labs after injecting analytics into
          experiments and lab.
       4. Prepare a consolidated lab status sheet consisting
          of deployment status, tag, date etc prepared - In
          progress


***** Deadline - March 2020
***** Assignee - Raj/ Pavan
       
*** Outreach Portal maintenance 
**** Completed 
     |------+------------+---------------------+---------|
     | S.No | Version No | Release Date        | Link    |
     |------+------------+---------------------+---------|
     |   1. | v2.9.0     | April 29th, 2019    | [[https://github.com/vlead/outreach-portal/releases/tag/v2.9.0][v2.9.0]]  |
     |   2. | v2.8.1     | February 22nd, 2019 | [[https://github.com/vlead/outreach-portal/releases/tag/v2.8.1][v2.8.1]]  |
     |   3. | v2.8.0     | February 21st, 2019 | [[https://github.com/vlead/outreach-portal/releases/tag/v2.8.0][v2.8.0]]  |
     |   4. | v3.0.0     | September 6th, 2019 | [[https://github.com/vlead/outreach-portal/releases/tag/v3.0.0][v3.0.0]]  |
     |   5. | v3.1.0     | October 17th, 2019  | [[https://github.com/vlead/outreach-portal/releases/tag/v3.1.0][v.3.1.0]] |
     |   6. | v3.2.0     | December 28th, 2019 | [[https://github.com/vlead/outreach-portal/releases/tag/v3.2.0][v.3.2.0]] |
     |------+------------+---------------------+---------|

**** Ongoing Tasks
     1. Update and host Outreach Portal User Manual on vlabs dev pages.
     2. Test environment set up and back scripts for
        Outreach Portal
     3. Update the scenario / use case document of Outreach Portal
     4. Review the above Outreach Portal scenario document -
        Ravi
     5. Bug fixes - [[https://github.com/vlead/outreach-portal/blob/approved-workshops/src/realization-plan/index.org#estimates][estimates]]

     Status as on Feb 5th 2020 - [[https://gitlab.com/vlead/work-logs/-/blob/master/src/staff/travula/2020/2020-feb.org][link]]

     - Plan for Feb :: 
  
**** Priority Tasks till end of Phase III ( March 2020)  
      - Maintain a steady stable state of Outreach Portal
        and the AWS hosted labs. 
      - No extra effort to be made on any development
        activities on the current Outreach Portal.
        Communicated to IITB on [[https://drive.google.com/open?id=19vsBdoQkF7cosjVGTVhw8UAFmcAJ_Xmv][mail]] on 3rd Oct, 2019
      - Only bug fixing and related activities will be
        supported till March 2020. Enhancement requests
        will not be entertained. Communicated to IIT D 
        by mail on ??? ( to be confirmed with Ravi Shankar)

**** Deadline - March 2020
**** Assignee - Thirumal 
*** Analytics & the works 
**** Reference - As per the [[https://drive.google.com/open?id=19P-3QakbEYzOxFtZE-Efl39XzjFThg7w][mail]] shared with Prof Rajan on
                 13th Jan 2020
**** Basic Analytics
     - Target :: To have basic analytics implemented on all
                 vlabs (about 90 currently) that are hosted
                 on AWS and accessible via vlab.co.in.
     - Definition of Basic Analytics :: Usage statistics per
          lab and per experiment + Total Users, New users
          and returning users (based on cookies not user
          logins) + State-wise distribution of users

     - Timeline :: About 65 labs by 1st Feb 2020 and
                   remaining 25 labs hopefully by 1st Mar
                   2020 or earlier. These 25 have some
                   problems with their build process which
                   needs to be fixed by the developing
                   institutes. We will inject the analytics
                   into these labs as and when we receive
                   the hosting requests of the fixed labs
                   from the developing institutes.Analytics
                   data will be available prospectively,
                   i.e., from the date of integration of the
                   analytics into the labs.

     - Status ::
                1. Injected basic analyics in 97 of 99 labs
                   hosted on AWS
                2. Shared [[https://drive.google.com/open?id=1pBiT-o9KieksKcQ-y_RpGKMi6fEKHN-y][status]] with IITD on 23rd Jan 2020
                3. Remaining 2 labs in progress
                4. Experiment level analytics and concept of
                   usage not supported.
                5. Dashboard to be created as per request
                   from IITD 

     - Plan for Feb ::
                1. Inject basic analytics in  all the 99
                   labs. 
                2. Inject analytics in Phase 3 experiments &
                   lab.
                3. Model for usage - sessions vs page views
                   etc implementation and interpretation. A
                   small report on the above. 
                
**** Single sign on  
      - Target :: Mandatory single sign-on to be implemented
                  (using swayam/gmail credentials) on
                  vlab.co.in. All past and new experiments
                  will have to support the login feature.

      - Timeline :: Prototype for one lab by 1st
                    Mar 2020. We will aim to finish all the
                    labs tentatively by 1st Jul 2020. We
                    will have a better idea once the
                    prototype is done.
      - Plan for Feb 2020  ::
                 1. Integrate single sign on module to
                    reverse proxy.
                 2. Confguring reverse proxy to use google
                    as SSO as identity provider.
                 3. Concept of User ID in user login  

**** Context storing 
      - Target :: Logging back in restores context (i.e.,
                  experiment) that the user was previously
                  doing.
      - Timeline :: Prototype by 1st Apr 2020. We will aim
                    to finish all the labs tentatively by
                    1st Aug 2020. We will have a better idea
                    once the prototype is done. 
**** User-specific Analytics 
      - Target :: To provide user specific analytics based
                  on user profile captured during
                  registration (which is separate from
                  login).

      - Timeline :: 1st May 2020 for a prototype. 1st Sep
                    2020 for all labs.

**** Deadline - Sept 2020
**** Assignee - Raj/Pavan/Krutham
       
** Phase III
*** Experiment Development
**** Completed 
    |------+----------+----------+----------+-------------|
    | S.No | # of exp | # of exp | # of exp | link        |
    |      | at 90 %  | at 85%   | at 70%   |             |
    |------+----------+----------+----------+-------------|
    |    1 | 8        | 15       |          | [[http://exp-iiith.vlabs.ac.in/][hosted link]] |
    |------+----------+----------+----------+-------------|

    - What is 90 % ?  Cross browser testing + fixing
                      User testing thro Outreach & 
                      Content/ aesthetics fixes pending
  
    - What is 85 % ?  Video recording + all of the above
                      pending

    - What is 70 % ? Final fixing + testing + all of the
                     above pending   
  
**** In Progress 
   
    
     - Plan for the month of Feb :: 
 
     1. Content Clean up - Balamma, Niranjan - EOD Feb 5th  

     2. Cross Browser Testing to be done for all the
        experiments (Fix issue in Radix Sort Experiment ) 
        - Balamma and Niranjan ( help from Raj) - 20th Feb 
   
     3. Footer service for the experiments Landing Page -
        Balamma - 29th Feb 
      
     4. Infra Fixes - Feedback ( active link +
        prev/next),  iFrame - Balamma - 29th Feb 

     5. Complete all Video Recordings - status[[https://docs.google.com/spreadsheets/d/1Y8LRBI9A0GaV8sxwiaNauDCOdjVKxZ2cZpriqYlBg8s/edit?ts=5e268196#gid=0][ link]] 
    
*** IIT Bombay Process 
**** Completed
     R1 submited for review for 23 experiments 
     R2 submitted for 20 experiments.
     [[https://gitlab.com/vlead-projects/experiments/ere/index/blob/develop/src/meeting-minutes/2019-08-20.org][Talk]] with Pushpdeep on development process ( IIT B) 
**** In Progress 

     - Plan for the month of Feb :: 

     Uploading of R2 content for 3 experiments to IITB
     portal   
    
*** Reviews of labs developed by IIT B
    [[https://gitlab.com/vlead/ldc/tree/master/lab-reviews/2019-08-17][Status link]]
