#+title:  VLEAD Project Status for the week of 16th Oct, 2017 
#+author: Priya Raman 
#+date: [2017-10-16 Mon]
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t
#+OPTIONS: arch:headline author:t c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil prop:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t title:t toc:nil
#+OPTIONS: todo:t |:t
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.3.1 (Org mode 8.3.4)

* Current VLEAD Projects
** Lalit 
*** Get IIITH Labs working on concerned faculty 
 * Deliverable : Set-up IIIT-Hyderabad labs on IIIT faculty machines.
 * Members :  Sravanthi M, Sravanthi B and Lalit
 * Status : 20 Profs ( for 27 labs ) have been contacted . 
            10 have responded. 
             4 set up ( 9 labs) have been done. 
 * [[https://gitlab.com/vlead-systems/iiith-labs-set-up/blob/master/src/index.org][Plan]] : Set up scheduled for this week - 1 labs ( 1 faculty)
 * Date of completion : 30th Oct 2017
 * To Do - Repository to capture all the feedback - Sravanthi 
         - Get the documentation of installation of Vagrant box in the
           vlead-onboarding repo / Virtual Labs landing page - Lalit
         - Set up and test the vagrant box on a mac/windows/upbuntu
           demo system in the lab and have a report detailing the issues if
           any.
*** Security Audit - Phase I
- Deliverable - 1. An audit report from CDAC
                2. Fixes by VLEAD on audit findings
- Members - CDAC Team, Lalit , Sravanthi ( fixes might be done by
  other members of VLEAD as and when needed) and Systems Team 
- [[ https://gitlab.com/vlead-systems/vlead-security-audit/blob/master/src/index.org][Status]] - Audit has started on Oct 3rd . Documents from VLEAD has
  been shared.  First report has been received from CDAC.             
- Payment to be released based on progress as per discussion with Lalit. 
- To Do - NDA to be signed by CDAC - Lalit 
        - Raise all the findings reported from CDAC as issues and fix them.
        - Get the tool/processes that CDAC uses for testing.
 
*** Communication
 - Follow up on Solan workshop - Lalit to follow up asking them
   for their experience.
 - Send IIT Delhi the college cloud HDD that they have requested.
   -    
*** IIITH Labs - Conversion from Flash to Javascript and fixing of bugs
 * Deliverable - Convert all the 37 IIITH labs to FOSS and have them
   working.
 * Members - Sravanthi.M ( moved to systems) and Sravanthi.B
 * [[https://github.com/vlead/iiith-labs-testing-and-fixing/blob/master/index.org][Status]] - 3 Labs in Flash to be converted to JS.
 * Date of completion - Dependent on resources . estimation of 2
   person weeks.
 * Resources needed - Thirumal to confirm 1-2 SSAD students for week.
** Lalit and Thirumal  
*** [[https://github.com/vlead/2017-monsoon-ssad-projects/blob/master/src/projects-allocated.org][SSAD]] projects  
*** College Cloud
- Deliverable - College cloud current status and
  draft of the requirements 
- Members - Lalit, Thirumal, resource to be identified
- Status - Demo and presentation on 2017-10-18 Wednesday.
** Thirumal 
*** Systems 
**** Cluster Automation 
 * Deliverable - Cluster Automation with OVPL setup Base II m/c
 * Members - Raghupathi 
 * [[https://gitlab.com/vlead-systems/cluster-automation/milestones/5][Status]] 
 * [[https://gitlab.com/vlead-systems/cluster-automation/blob/openvz-ovpl/src/realization-plan/index.org][Plan]] 
 * Date of completion - 14th Oct, 2017    
**** PXE Server Setup 
 * Deliverable - 
 * Members - Prakash
 * [[ https://gitlab.com/vlead-systems/docs/milestones/1][Status]]
 * Plan 
 * Date of completion - 13th Oct 2017  
**** Cloud Bootstrapping Automation 
 * Deliverable - Automating boot strapping process of the VLEAD's
   infrastructure on AWS
 * Members - Siva
 * [[https://gitlab.com/vlead-systems/cloud-infrastructure-automation/milestones/1][Status]]
 * [[https://gitlab.com/vlead-systems/cloud-infrastructure-automation/blob/master/src/realization-plan/index.org][Plan]] 
 * Date of completion - 27th Oct, 2017

**** Experiment server
 * Deliverable  : Oct 10, 2017
 * Members : Sravanthi M 
 * [[https://gitlab.com/vlead-systems/experiment-server/milestones/1][Status]]
 * [[https://gitlab.com/vlead-systems/experiment-server/blob/master/src/realization-plan/index.org][Plan]]
 * Date of completion - 13th Oct 2017 

**** College Cloud Running at all times at VLEAD 
1. Can we get it running on a 32 GB laptop and take it for workshops?
2. 

*** Services
**** Analytics Dashboard 
 * Deliverable - Visualization of feedback analytics
 * Members - Reena, Mrudvika ( currently working on VLABS landing
   pages)
 * [[https://github.com/vlead/analytics-dashboard/milestone/2][Status]] 
 * [[https://github.com/vlead/analytics-dashboard/blob/latest-new/src/realization-plan/index.org#plan-milestone][Plan]]
 * Date of completion - 9th Oct, 2017

**** Lab Data Service
 - Deliverables: Updating LDS with 177 Labs Data and adding new
   Features and Enhancements.
 - Members : Madhavi, Mrudhvika and Sanchita
 - [[https://github.com/vlead/lab-data-service/milestone/8][Status]] 
 - [[https://github.com/vlead/lab-data-service/blob/phase/src/realization-plan/index.org][Plan]]
 - Date of completion - 13th October, 2017

**** VLABS Landing Page
 * Deliverable - Vlabs landing page dynamically generated (using LDS )
   with search feature
 * Members - Madhavi, Mrudvika, Sanchita (partly), Sripathi and
   Balamma
 * [[https://github.com/vlead/vlabs-landing-pages/milestone/5][Status]]
 * [[https://github.com/vlead/vlabs-landing-pages/blob/styles/src/realization-plan/index.org#milestone-two][Plan]]
 * Date of completion of Milestone 2 : 31st Oct, 2017

**** Refactoring of Analytics services 
 * Deliverable - Provide a set of APIs to get feedback, user and usage
   analytics for online and college cloud edition of Virtual Labs.
 * Members - Sripathi
 * [[https://github.com/vlead/vlabs-analytics-service/milestone/1][Status]] ( API), [[https://github.com/vlead/analytics-db/milestones][Status]] (Database service)
 * [[https://github.com/vlead/vlabs-analytics-service/blob/analytics-apis/src/realization-plan/index.org][Plan]] (API), Plan ( Database service) 
 * Date of completion - 27th Sept, 2017
 * Steps ahead - 1. Frontend development ( Analytics Dashboard )
                 2. Requirements for the dashboard.   
                 3. Analytics Integration 
 * Demo - 16th Oct 2017

*** Demo of till date work done on platform realization
   + Demo of the current Status :
     1. College Cloud
     2. Authoring Platform
     3. APIs (for state saving)
     4. Analytics 
    
   + Can we port the IIITH labs to the platform that we are trying to
     propose ? 
     - Deliverable - A demo from Thirumal 

   + Pending work for the Analytics framework -
     - What is the data that we want to capture.
     - Identify the stakeholders and understand what is the data that
       they want.
     - Define a analytics model that addresses the needs. 
     - Deliverable - What is the analytics of Virtual Labs - A
       document - a first draft to be presented on 16th Oct 2017 of
       requirements of basic reports and analytics.
   
 - Deliverable - Demo of work done so far on platform 
 - Members -  Thirumal 
 - Date of demo - 16th Oct 2017
   
** Priya 
*** HR Policy Document
- Deliverable - Presentation of a [[https://gitlab.com/vlead/work-logs/blob/master/src/staff/docs/vlead-hr-policy.org][employee]] handbook detailing VLEAD's
  policy, processes, and benefits.
- Members - Ravi, Priya and Ravi Kiran 
- Date of demo - 14th Oct 2017
*** Road plan of broad areas for Phase 3  
 - Deliverable - Road Plan for Phase 3 
 - Members - Ravi and Priya 
 - Status - Presentations of current status and requirements under way
  to kick start the above task.
 - Broad Areas identified : 
**** College Cloud
**** Analytics 
**** Platorm and Lab Development 
**** Outreach 

** Ravi
*** Digital Classroom 
Deliverable - Iron out the last minute wrinkles and get the Digital lab up and running. 
Members - Ravi and Kiran
Date of demo - 14th Oct 2017

*** Outreach for Phase 3
- Deliverable - Outreach Model for Phase 3  
** Prof Venkatesh 
*** Papers 
 - Experiment as a service to be converted into paper - Thirumal 
 - Feedback of Virtual Labs - Ravi 
 - Presentation of the work critiquing of the Virtual Labs - Mr. Mrityunjaya
 - Tech paper on New Architecture of Virtual Labs emphasizing problems with the old one. - Prof Venkatesh

*** Phase 3 Proposal 

* In pipeline
** Capture end users requirements
*** End users requirements  
 - What will each end user will need ? 
 - A plan to realize it in the next couple of months. 
 - Every end user events to be captured for analytics. 
 - End users : MHRD, Students, Outreach, Lab developers.
 - Analytics, Visualization and Report Generation for each category of
   end user.
 - Analytics framework - Data acquisition, analysis and presentation.
 - What does each stakeholder want ? 
 - What is an experiment ? - Something that is done by a student that
   involves interaction. It is not a reading , solving a quiz or
   watching a video. The student has to perform an action. Involves
   interaction from the end user . Come up with a report at the end of
   the experiment. What does virtualization offer to the students ? 
 - A general paper on Virtual Labs with its objective, stake holder
   requirements, a technical report that answers the above questions - 
   Lalit and Mr. Mritunjaya 

*** Creating requirements of Virtual Labs repository  
 - 1. Authoring 
 - 2. Analytics
 - 3. Platform
 - 4. System
 - 5. College Cloud 

** Define lab development process
 - New Lab development :   1. What is the development model  ?
                           2. Do we have a LDK for the phase 3 developers ? 
                           3. What is our authoring tool ?

** Analytics Framework
** Hiring MSIT interns 
 - Start on the process.
 - Lalit and Priya to talk to Devi Prasad regarding this as done
   in 2016. Shelved till Nov for clarity of Phase 3. 


** Decisions on project deliverables 
- Get the deliverables to be a paper that can be submitted . 
- Identify the first author who will be the owner.

* Completed VLEAD Projects
** OpenVZ Template Creation ( Base m/c )
 * Deliverable - Template for creating a container and installing
   packages on Base IV m/c.
 * Members - Prakash under the guidance of Siva
 * [[https://gitlab.com/vlead-systems/docs/blob/master/src/base-machines-docs/create-new-template-from-existing.org][Status]] - Done

** Migration of VLEAD systems project
- Deliverable : Migration of systems project from Bitbucket and Github
  to Gitlab
- Members : Raghupathi
- [[https://gitlab.com/vlead-systems/systems-operations/milestones/1][Status]]  : Done

** Creation of AWS EC2 instance and AMI image from it.
- Deliverable : Make a document for creation of AWS EC2 instance and
  AMI image from it.
- Members : Raghupathi
- [[https://gitlab.com/vlead-systems/systems-operations/milestones/5][Status]] 

** Build Vlabs on Open edX
 * Deliverable - Automating cloning edx repos from vlabs-on-openedX
   organization on Gitlab, update them and commit them back.
 * Members - Sravanthi M
 * [[https://gitlab.com/vlead-systems/build-vlabs-on-openedx/milestones/1][Status]] - Done 
 * [[https://gitlab.com/vlead-systems/build-vlabs-on-openedx/blob/master/src/realization-plan/index.org][Plan]] 
 * Date of Completion - 18th Sept, 2017
* Paused Projects 
** Petition to buy software licenses and hardware for VLEAD
 - Deliverable - Petition with list of software , hardware and to dos
   for Mr. Bhaskar Prasad.  A petition for only Matlab and simulink
   was sent on 15th Sept as requested by Mr. Bhaskar Prasad.
 - Members - Priya, Maruthi and Sanchita ( partly)
 - [[https://gitlab.com/vlead/purchases/blob/master/src/software/2017-08-28-licenses/petition.org][Status]] - Petition [[https://gitlab.com/vlead/purchases/issues/30][sent]] on 13th Sept to Mr Bhaskar Prasad for
   quotations. *Shelved* 
 - Date of completion  - 13th Sept 2017

** Petition to buy office supplies for VLEAD
 - Deliverable - Petitions with list of office supplies
 - Members - Ravi Kiran
 - [[https://gitlab.com/vlead/purchases/tree/master/src/equipment/2017-06-14-office-supplies][Status]] - Petitions prepared. *Shelved*
 - Date of completion  - 15th Sept 2017
