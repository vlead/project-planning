#+TITLE: Presentation by Microsoft
#+AUTHOR: VLEAD
#+DATE: [2018-01-25 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This captures some of the items talked about during the
  presentation by Microsoft.

* How to collaborate
** Partnership
  1. Industry can use the virtual lab platform
  2. ARM and other companies contribute to the platform
  3. What is missing is presenting material so that teachers
     are unable to present during the educational program.
     Can a teacher present the material in a way that is
     understood by the students?
  4. Can a problem be analysed at the industrial scale or
     understand the concepts or improve the foundational
     standing.

** Scope of Collaboration
   1. Host on Azure.
   2. Improve the quality of labs.
   3. Take VLABS to colleges. 

* Architecture Presentation
  1. The design presented is learned from the frameworks
     like kubernetics, etc. 
  2. Docker Architecture is presented.
  3. A move from system containers (openVZ) to application
     containers(docker) is made.  This will help reduce
     final package size where the redundancies in OS, N/W
     components.

* Action Items
  1. Understand the clear support from MS for building the
     infrastructure.
  2. How virtual labs can be taken to the colleges?  The
     numbers will come from VLEAD, and MS will come with a
     plan of shared responsibilities.
  3. Build Open Source Community, with the core team being
     at VLEAD.
